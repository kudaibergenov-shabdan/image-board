import {Route, Switch} from "react-router-dom";
import Layout from "./UI/Layout/Layout";
import Home from "./containers/Home/Home";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Home} />
        </Switch>
    </Layout>
    )
;

export default App;
