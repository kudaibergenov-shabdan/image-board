import {FETCH_LAST_MESSAGES_SUCCESS, FETCH_MESSAGES_SUCCESS} from "../actions/messagesAction";

const initialState = {
    messagesArray: []
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messagesArray: action.payload}
        case FETCH_LAST_MESSAGES_SUCCESS:
            return {...state, messagesArray: [...state.messagesArray].concat(action.payload)};
        default:
            return state;
    }
};

export default messagesReducer;