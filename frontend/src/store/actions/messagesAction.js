import axios from "axios";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const FETCH_LAST_MESSAGES_REQUEST = 'FETCH_LAST_MESSAGES_REQUEST';
export const FETCH_LAST_MESSAGES_SUCCESS = 'FETCH_LAST_MESSAGES_SUCCESS';
export const FETCH_LAST_MESSAGES_FAILURE = 'FETCH_LAST_MESSAGES_FAILURE';

const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, payload: messages});
const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

const fetchLastMessagesRequest = () => ({type: FETCH_LAST_MESSAGES_REQUEST});
const fetchLastMessagesSuccess = lastMessages => ({type: FETCH_LAST_MESSAGES_SUCCESS, payload: lastMessages});
const fetchLastMessagesFailure = () => ({type: FETCH_LAST_MESSAGES_FAILURE});

export const fetchMessages = () => {
    return async dispatch => {
        try {
            dispatch(fetchMessagesRequest());
            const response = await axios.get('http://localhost:8000/messages');
            dispatch(fetchMessagesSuccess(response.data));
            dispatch(fetchLastMessage(response.data[response.data.length - 1]));
        } catch (e) {
            if (e.response) {
                if (e.response.status === 400) {
                    setTimeout(() => {
                        dispatch(fetchMessages());
                    }, 5000);
                }
            } else {
                dispatch(fetchMessagesFailure());
            }
        }
    };
};

export const fetchLastMessage = (lastMessage) => {
    return async dispatch => {
        try {
            dispatch(fetchLastMessagesRequest());
            const response = await axios.get(`http://localhost:8000/messages?datetime=${lastMessage.datetime}`);
            if (response.data.length > 0) {
                dispatch(fetchLastMessagesSuccess(response.data));
                setTimeout(() => {
                    dispatch(fetchLastMessage(response.data[response.data.length - 1]));
                }, 5000);
            } else {
                setTimeout(() => {
                    dispatch(fetchLastMessage(lastMessage));
                }, 5000);
            }
        } catch (e) {
            dispatch(fetchLastMessagesFailure());
        }
    };
};

export const createMessage = messageData => {
    return async () => {
        try {
            await axios.post('http://localhost:8000/messages', messageData);
        } catch (e) {
            console.log('Error while sending message to server:', e);
        }
    };
};