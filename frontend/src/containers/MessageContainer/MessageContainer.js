import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMessages} from "../../store/actions/messagesAction";
import Message from "../../components/Message/Message";
import {Grid, makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    space: {
        marginTop: "30px",
        border: "2px solid grey",
        borderRadius: "4px",
    }
});

const MessageContainer = () => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages.messagesArray);

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            className={classes.space}
        >
            <Grid item>
                {messages.map(message => (
                    <Message
                        key={message.id}
                        messageObj={message}
                    />
                )).reverse()}
            </Grid>

        </Grid>

    );
};

export default MessageContainer;