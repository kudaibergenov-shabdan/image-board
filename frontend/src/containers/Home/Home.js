import React from 'react';
import MessageSender from "../MessageSender/MessageSender";
import MessageContainer from "../MessageContainer/MessageContainer";

const Home = () => {
    return (
        <>
            <MessageSender/>
            <MessageContainer/>
        </>
    );
};

export default Home;