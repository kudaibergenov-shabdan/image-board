import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";
import FileInput from "../../UI/FileInput/FileInput";
import {createMessage} from "../../store/actions/messagesAction";
import {useDispatch} from "react-redux";

const useStyles = makeStyles({
   space: {
       marginTop: "5px",
   }
});

const MessageSender = () => {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [state, setState] = useState({
        author: "",
        message: "",
        image: null,
    });

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };

    const onSubmit = messageData => {
        dispatch(createMessage(messageData));
        setState(prev => ({
            ...prev,
            message: "",
            image: ""
        }));
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file}
        });
    };

    return (
        <Grid
            container
            direction="column"
            component="form"
            spacing={2}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <Grid container spacing={2} className={classes.space}>
                <Grid item xs>
                    <TextField
                        fullWidth
                        required
                        variant="outlined"
                        label="Message"
                        name="message"
                        value={state.message}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <FileInput
                        label="Image"
                        name="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} alignItems="center" className={classes.space}>
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Author: Anonymous"
                        name="author"
                        value={state.author}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <Button
                        type="submit"
                        color="primary"
                        variant="contained"
                    >
                        Send
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default MessageSender;