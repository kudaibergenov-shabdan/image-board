import React from 'react';
import './Message.css';
import {apiURL} from "../../config";

const Message = ({messageObj}) => {
    const fileServer = apiURL + '/uploads/';

    return (
        <div className="Message">
            <p className="Message-paragraph">
                <span>{(messageObj.author) ? messageObj.author : "Anonymous"} said ({messageObj.datetime}):</span>
            </p>
            <div className="Message-block">
                {(messageObj.image) ?
                    <img src={fileServer + messageObj.image} alt="png" width="80px" height="80px"/>
                    : null}
                <span className="Message-text">{messageObj.message}</span>
            </div>
        </div>
    );
};

export default Message;