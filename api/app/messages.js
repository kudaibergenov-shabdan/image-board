const express = require('express');
const fileDb = require('../fileDb');
const {nanoid} = require("nanoid");
const multer = require('multer');
const config = require('../config');
const path = require("path");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();
router.get('/', (req, res) => {
    if (!req.query.datetime) {
        const messages = fileDb.getItems();
        if (messages.length > 0) {
            return res.send(messages);
        } else {
            return res.status(400).send({"error" : "No data found"});
        }
    } else {
        if (!isNaN(new Date(req.query.datetime))) {
            const message = fileDb.getItem(req.query.datetime);
            if (message) {
                return res.send(message)
            } else {
                return res.status(400).send({"error": "No messages with such a datetime"});
            }
        } else {
            return res.status(400).send({"error" : "Invalid datetime"});
        }
    }
});

router.post('/', upload.single('image'), (req, res) => {
    if (!req.body.message) {
        return res.status(400).send({error: "Incorrect request. Message field does not exist"});
    }

    const message = {
        id: nanoid(),
        datetime: (new Date).toISOString(),
        message: req.body.message
    };

    if (req.file) {
        message.image = req.file.filename;
    }
    if (req.body.author) {
        message.author = req.body.author;
    }
    const newMessage = fileDb.addItem(message);
    res.send(newMessage);
});

module.exports = router;
